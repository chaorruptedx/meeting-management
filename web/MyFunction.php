<?php

	//function debugging and die
    function dd($data, $die=true)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";

        die();
    }

    //function debugging only
    function d($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
