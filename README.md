# Meeting Management


## Requirements

* Apache
* PHP 5 / PHP 7
* MySQL
* Composer - https://getcomposer.org/
* Git - https://git-scm.com/
* Brain


## Installation

* Download source code => ```git clone https://gitlab.com/chaorruptedx/meeting-management.git```
* Open terminal and type => ```composer update```
* Import database from `database-sql/meeting_management.sql` into your MySQL
* Update database configuration. Edit the file `config/db.php` with real data, for example:
```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=meeting_management',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```