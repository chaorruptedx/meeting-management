<?php

namespace app\controllers;

use Yii;
use app\models\Event;
use app\models\CategoryEvent;
use app\models\EventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $searchModel->status = '1';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Event();
        $modelCategoryEvent = new CategoryEvent();

        if ($model->load(Yii::$app->request->post())) {

            $model->status = '1';

            $model->save();

            if ($modelCategoryEvent->load(Yii::$app->request->post())) {

                foreach ($modelCategoryEvent->id_categories as $row) {

                    $modelCategoryEventSave = new CategoryEvent();

                    $modelCategoryEventSave->id_category = $row;
                    $modelCategoryEventSave->id_event = $model->id;
                    $modelCategoryEventSave->status = '1'; // Active

                    $modelCategoryEventSave->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelCategoryEvent' => $modelCategoryEvent,
        ]);
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id); // Event

        $modelCategoryEvent = new CategoryEvent(); // Pakai dekat form, panggil data lama

        $data_lama_banyak = CategoryEvent::find()
            ->where(['id_event' => $model->id, 'status' => '1'])
            ->all(); // Panggil data lama

        foreach ($data_lama_banyak as $value) {

            $modelCategoryEvent->id_categories[] = $value->id_category;
        }

        if ($model->load(Yii::$app->request->post())) {

            $model->status = '1';

            $model->save();

            $data_lama_banyak_buang = CategoryEvent::find()
                ->where(['id_event' => $model->id, 'status' => '1'])
                ->all(); // Panggil data lama untuk buang

            foreach ($data_lama_banyak_buang as $value) {

                $value->status = '-1';

                $value->save();
            }

            if ($modelCategoryEvent->load(Yii::$app->request->post())) {

                foreach ($modelCategoryEvent->id_categories as $row) {

                    $modelCategoryEventSave = new CategoryEvent();

                    $modelCategoryEventSave->id_category = $row;
                    $modelCategoryEventSave->id_event = $model->id;
                    $modelCategoryEventSave->status = '1'; // Active

                    $modelCategoryEventSave->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelCategoryEvent' => $modelCategoryEvent,
        ]);
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $model->status = '-1';

        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
