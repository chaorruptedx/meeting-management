<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Category;
use app\models\Bankstatus;

use kartik\time\TimePicker;
use kartik\date\DatePicker;
use kartik\select2\Select2;



/* @var $this yii\web\View */
/* @var $model app\models\Event */
/* @var $form yii\widgets\ActiveForm */

// $modelCategory = Category::find()
//     ->where(['status' => '1'])
//     ->all();

$listCategory = Bankstatus::listCategory();
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($modelCategoryEvent, 'id_categories')->widget(Select2::classname(), [
        'data' => $listCategory,
        'options' => [
            'placeholder' => '',
            'multiple' => true
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textArea(['maxlength' => true]) ?>

    <?//= $form->field($model, 'time')->textInput() ?>

    <?php echo $form->field($model, 'time')->widget(TimePicker::classname(), [
        pluginOptions => [
            'showMeridian' => false,
        ]
    ]); ?>

    <?//= $form->field($model, 'date')->textInput() ?>

    <?php echo $form->field($model, 'date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Date ...'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]); ?>

    <?//= $form->field($model, 'status')->textInput() ?>

    <?//= $form->field($model, 'created_at')->textInput() ?>

    <?//= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
