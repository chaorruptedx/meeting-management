<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Event'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'header' => 'No.',
                'class' => 'yii\grid\SerialColumn'
            ],


            [
                'label' => 'Category',
                // 'attribute' => ''
                'value' => function ($model) {

                    foreach ($model->categoryEvents as $value) {

                        $data .= $value->category->category.'<br>'; // CONCAT to combine STRING
                    }

                    return $data;
                },
                'format' => 'html',
            ],





            // 'id',
            // 'id_category',
            // [
            //     'label' => 'Category',
            //     // 'label' => $searchModel->getAttributeLabel('id_category'),
            //     // 'attribute' => 'category_name',
            //     'attribute' => 'id_category',
            //     // 'value' => 'category.adcategory',
            //     'value' => function ($model) {

            //         $category = $model->category->category;

                    

            //         return '1.'.$category;
            //     },
            // ],
            'title',
            'time',
            'date',
            //'status',
            //'created_at',
            //'updated_at',

            [
                'header' => 'Action',
                'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
